import { pactWith } from 'jest-pact';
import { Matchers } from '@pact-foundation/pact';

import * as MOVIE_DATA from './fixtures/movie-details.json';
import { getDetails } from './get-details';

pactWith(
  { consumer: 'tmdb web client', provider: 'tmdb api movies endpoint' },
  (provider) => {
    describe('GET movie details endpoint', () => {
      const movieDetailsSuccessResponse = {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
        body: MOVIE_DATA,
      };

      const movieDetailsRequest = {
        uponReceiving: "a request for a movie's details",
        withRequest: {
          method: 'GET',
          path: '/movie/{movie_id}',
        },
      };

      beforeEach(() => {
        const interaction = {
          state: 'i have a list of movies',
          ...movieDetailsRequest,
          willRespondWith: movieDetailsSuccessResponse,
        };
        return provider.addInteraction(interaction);
      });

      // add expectations
      it('returns a successful body', () => {
        return getDetails({
          url: provider.mockService.baseUrl,
        }).then((movies) => {
          expect(movies).toEqual(MOVIE_DATA);
        });
      });
    });
  }
);

describe('GET movie details by id', () => {
  it('should work', () => {
    expect(getDetails()).toEqual('data-access-movies');
  });
});
