export async function getDetails(movieID) {
  const request = new Request(
    `https://api.themoviedb.org/3/movie/${movieID}?&api_key=cfe422613b250f702980a3bbf9e90716`,
    {
      method: 'GET',
    }
  );
  const response = await fetch(request);
  return response.json();
}
