# data-access-movies

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build data-access-movies` to build the library.

## Running unit tests

Run `nx test data-access-movies` to execute the unit tests via [Jest](https://jestjs.io).
